<?php
/**
 * Created by PhpStorm.
 * User: ernesto
 * Date: 6/8/2019
 * Time: 08:51
 */

namespace App\Repository;


use Doctrine\ORM\EntityRepository;

class eliminarPaquetesVencidos extends EntityRepository
{
    public function deleteAllVencidos(\DateTime $fecha){
        $fecha2 = new \DateTime();
        $fecha2->add(new \DateInterval('PT24H'));
        $qb=$this->createQueryBuilder('p');
        $qb->delete()->where('p.vence BETWEEN  :fecha1 and :fecha2')
            ->setParameter('fecha1',$fecha)
            ->setParameter('fecha2',$fecha2);
        return $qb->getQuery()->getResult();
    }

}