<?php
/**
 * Created by PhpStorm.
 * User: ernesto
 * Date: 29/7/2019
 * Time: 01:30
 */

namespace App\Repository;


use App\Entity\User;
use Doctrine\ORM\EntityRepository;

class TransferenciasRepository extends EntityRepository
{
    public function getAllTransfers(User $emisor, User $receptor){
        $qb=$this->createQueryBuilder('a');
        $qb->select()
            ->where('a.emisor=:emisor')
            ->orWhere('a.receptor=:receptor')
            ->setParameter('emisor', $emisor)
            ->setParameter('receptor', $receptor);
        return $qb->getQuery()->getResult();
    }
    public function getTransferenciasPeriodo(User $emisor, \DateTime $f1, \DateTime $f2){
        $qb=$this->createQueryBuilder('a');
        $qb->select('sum(a.saldo)')
            ->where('a.emisor=:emisor and a.fecha BETWEEN  :fecha1 and :fecha2')
            ->setParameter('emisor', $emisor)
            ->setParameter('fecha1', $f1)
            ->setParameter('fecha2', $f2);
        return $qb->getQuery()->getResult();
    }
}