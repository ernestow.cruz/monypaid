<?php

namespace App\Form;

use App\Entity\Tutoriales;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TutorialesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha',DateType::class,array('label'=>'Fecha', 'widget'=>'single_text', 'attr'=>['class'=>'datepicker']))
            ->add('tutorial')
            ->add('web')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tutoriales::class,
        ]);
    }
}
