<?php

namespace App\Form;

use App\Entity\Testimonios;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TestimoniosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha',DateType::class,array('label'=>'Fecha', 'widget'=>'single_text', 'attr'=>['class'=>'datepicker']))
            ->add('watsapp')
            ->add('testimonio')
            ->add('user')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Testimonios::class,
        ]);
    }
}
