<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('email')
            ->add('password')
            ->add('saldocuc',TextType::class,array('label'=>'Saldo CUCQ3'))
            ->add('saldousd',TextType::class,array('label'=>'Saldo USD'))
            ->add('ganancia')
            ->add('dias_transferencias',TextType::class,array('label'=>'Valida por (x dias)'))
            ->add('transferencia_periodo',TextType::class,array('label'=>'Transferencia que puede realizar'))
            ->add('fechatransferencia',DateType::class,array('label'=>'Fecha en que inicia', 'widget'=>'single_text', 'attr'=>['class'=>'datepicker']))


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
