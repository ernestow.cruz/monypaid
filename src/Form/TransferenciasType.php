<?php

namespace App\Form;

use App\Entity\Transferencias;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TransferenciasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha',DateType::class,array('label'=>'Fecha', 'widget'=>'single_text', 'attr'=>['class'=>'datepicker']))
            ->add('saldo')
            ->add('emisor')
            ->add('receptor')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Transferencias::class,
        ]);
    }
}
