<?php

namespace App\Form;

use App\Entity\MisPaquetes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MisPaquetesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ganancia')
            ->add('ganancia_acumulda')
            ->add('fecha',DateType::class,array('label'=>'Fecha', 'widget'=>'single_text', 'attr'=>['class'=>'datepicker']))
            ->add('vence',DateType::class,array('label'=>'Vence', 'widget'=>'single_text', 'attr'=>['class'=>'datepicker']))
            ->add('paquete')
            ->add('user')
        ;
    }
//
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MisPaquetes::class,
        ]);
    }
}
