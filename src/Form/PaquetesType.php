<?php

namespace App\Form;

use App\Entity\Paquetes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaquetesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('referidos')
            ->add('saldo')
            ->add('ganancia')
            ->add('ganancia_mes')
            ->add('dias_vida')
            ->add('oferta')
            ->add('hora',TextType::class,array('label'=>'Acumula cada'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Paquetes::class,
        ]);
    }
}
