<?php
/**
 * Created by PhpStorm.
 * User: ernesto
 * Date: 28/7/2019
 * Time: 09:19
 */

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class MisPaquetes
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\eliminarPaquetesVencidos")
 * @ORM\Table(name="mispaquetes")
 *
 */
class MisPaquetes
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\ManyToOne(targetEntity="Paquetes", inversedBy="mis_paquetes")
     * @ORM\JoinColumn(name="paquetes_id", referencedColumnName="id",onDelete="CASCADE"))
     */
    protected $paquete;
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="mispaquetes")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE"))
     */
    protected $user;
    /**
     * @ORM\Column(type="float")
     */
    protected $ganancia=0;
    /**
     * @ORM\Column(type="float")
     */
    protected $ganancia_acumulda=0;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $fecha;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $vence;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $fecha_cal;
    /**
     * MisPaquetes constructor.
     */
    public function __construct()
    {
        $this->fecha = new \DateTime();
        $this->vence = new \DateTime();
        $this->fecha_cal = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }


    /**
     * @return mixed
     */
    public function getPaquete()
    {
        return $this->paquete;
    }

    /**
     * @param mixed $paquete
     */
    public function setPaquete($paquete): void
    {
        $this->paquete = $paquete;
    }

    /**
     * @return mixed
     */
    public function getGanancia()
    {
        return $this->ganancia;
    }

    /**
     * @param mixed $ganancia
     */
    public function setGanancia($ganancia): void
    {
        $this->ganancia = $ganancia;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha): void
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getVence()
    {
        return $this->vence;
    }

    /**
     * @param mixed $vence
     */
    public function setVence($vence): void
    {
        $this->vence = $vence;
    }

    /**
     * @return mixed
     */
    public function getGananciaAcumulda()
    {
        return $this->ganancia_acumulda;
    }

    /**
     * @param mixed $ganancia_acumulda
     */
    public function setGananciaAcumulda($ganancia_acumulda): void
    {
        $this->ganancia_acumulda = $ganancia_acumulda;
    }

    /**
     * @return mixed
     */
    public function getFechaCal()
    {
        return $this->fecha_cal;
    }

    /**
     * @param mixed $fecha_cal
     */
    public function setFechaCal($fecha_cal): void
    {
        $this->fecha_cal = $fecha_cal;
    }

}