<?php
/**
 * Created by PhpStorm.
 * User: ecruz
 * Date: 9/7/2019
 * Time: 12:15
 */


// src/Entity/User.php

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    protected $patrocinador;

    /**
     * @ORM\Column(type="float")
     */
    protected $saldousd = 0;
    /**
     * @ORM\Column(type="float")
     */
    protected $saldocuc = 0;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $pais;
    /**
     * @ORM\OneToMany(targetEntity="MisPaquetes", mappedBy="user")
     */
    protected $mispaquetes;
    /**
     * @ORM\OneToMany(targetEntity="Testimonios", mappedBy="user")
     */
    protected $mistestimonios;

    /**
     * @ORM\OneToMany(targetEntity="Transferencias", mappedBy="emisor")
     */
    protected $transfemitidas;
    /**
     * @ORM\OneToMany(targetEntity="Transferencias", mappedBy="receptor")
     */
    protected $transfrecibidas;
    /**
     * @ORM\OneToMany(targetEntity="Ventas", mappedBy="user")
     */
    protected $ventas;

    /**
     * @ORM\Column(type="float")
     */
    protected $ganancia = 0;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    protected $dias_transferencias=7;
    /**
     * @ORM\Column(type="float")
     */
    protected $transferencia_periodo=50;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $fechatransferencia;

    /**
     * @return mixed
     */
    public function getFechatransferencia()
    {
        return $this->fechatransferencia;
    }

    /**
     * @param mixed $fechatransferencia
     * @return User
     */
    public function setFechatransferencia($fechatransferencia)
    {
        $this->fechatransferencia = $fechatransferencia;
        return $this;
    }
    /**
     * @return mixed
     */

    public function getDiasTransferencias()
    {
        return $this->dias_transferencias;
    }

    /**
     * @param mixed $dias_transferencias
     * @return User
     */
    public function setDiasTransferencias($dias_transferencias)
    {
        $this->dias_transferencias = $dias_transferencias;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVentas()
    {
        return $this->ventas;
    }

    /**
     * @param mixed $ventas
     * @return User
     */
    public function setVentas($ventas)
    {
        $this->ventas = $ventas;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTransferenciaPeriodo()
    {
        return $this->transferencia_periodo;
    }

    /**
     * @param mixed $transferencia_periodo
     * @return User
     */
    public function setTransferenciaPeriodo($transferencia_periodo)
    {
        $this->transferencia_periodo = $transferencia_periodo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGanancia()
    {
        return $this->ganancia;
    }

    /**
     * @param mixed $ganancia
     * @return User
     */
    public function setGanancia($ganancia)
    {
        $this->ganancia = $ganancia;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMistestimonios()
    {
        return $this->mistestimonios;
    }

    /**
     * @param mixed $mistestimonios
     * @return User
     */
    public function setMistestimonios($mistestimonios)
    {
        $this->mistestimonios = $mistestimonios;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTransfemitidas()
    {
        return $this->transfemitidas;
    }

    /**
     * @param mixed $transfemitidas
     * @return User
     */
    public function setTransfemitidas($transfemitidas)
    {
        $this->transfemitidas = $transfemitidas;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTransfrecibidas()
    {
        return $this->transfrecibidas;
    }

    /**
     * @param mixed $transfrecibidas
     * @return User
     */
    public function setTransfrecibidas($transfrecibidas)
    {
        $this->transfrecibidas = $transfrecibidas;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMispaquetes()
    {
        return $this->mispaquetes;
    }

    /**
     * @param mixed $mispaquetes
     * @return User
     */
    public function setMispaquetes($mispaquetes)
    {
        $this->mispaquetes = $mispaquetes;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getSaldousd()
    {
        return $this->saldousd;
    }

    /**
     * @param mixed $saldousd
     */
    public function setSaldousd($saldousd): void
    {
        $this->saldousd = $saldousd;
    }

    /**
     * @return mixed
     */
    public function getSaldocuc()
    {
        return $this->saldocuc;
    }

    /**
     * @param mixed $saldocuc
     */
    public function setSaldocuc($saldocuc): void
    {
        $this->saldocuc = $saldocuc;
    }

    /**
     * @return mixed
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * @param mixed $pais
     */
    public function setPais($pais): void
    {
        $this->pais = $pais;
    }

    /**
     * @return mixed
     */
    public function getWhatsapp()
    {
        return $this->whatsapp;
    }

    /**
     * @param mixed $whatsapp
     */
    public function setWhatsapp($whatsapp): void
    {
        $this->whatsapp = $whatsapp;
    }
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $whatsapp;

    /**
     * @return mixed
     */
    public function getPatrocinador()
    {
        return $this->patrocinador;
    }

    /**
     * @param mixed $patrocinador
     */
    public function setPatrocinador($patrocinador): void
    {
        $this->patrocinador = $patrocinador;
    }

    public function __construct()
    {
        parent::__construct();

    }
}