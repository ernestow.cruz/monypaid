<?php
/**
 * Created by PhpStorm.
 * User: ernesto
 * Date: 28/7/2019
 * Time: 09:56
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Transferencias
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\TransferenciasRepository")
 * @ORM\Table(name="transferencias")
 *
 */
class Transferencias
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="transfemitidas")
     * @ORM\JoinColumn(name="emisor", referencedColumnName="id",onDelete="SET NULL"))
     */
    protected $emisor;
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="transfrecibidas")
     * @ORM\JoinColumn(name="receptor", referencedColumnName="id",onDelete="SET NULL"))
     */
    protected $receptor;
    /**
     * @ORM\Column(type="string")
     */
    protected $estado='en espera';

    /**
     * @ORM\Column(type="datetime")
     */
    protected $fecha;
    /**
     * @ORM\Column(type="float")
     */
    protected $saldo=0;


    /**
     * @return mixed
     */
    public function getEmisor()
    {
        return $this->emisor;
    }

    /**
     * @param mixed $emisor
     * @return Transferencias
     */
    public function setEmisor($emisor)
    {
        $this->emisor = $emisor;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReceptor()
    {
        return $this->receptor;
    }

    /**
     * @param mixed $receptor
     * @return Transferencias
     */
    public function setReceptor($receptor)
    {
        $this->receptor = $receptor;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado): void
    {
        $this->estado = $estado;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha): void
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getSaldo()
    {
        return $this->saldo;
    }

    /**
     * @param mixed $saldo
     */
    public function setSaldo($saldo): void
    {
        $this->saldo = $saldo;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
    /**
     * Transferencias constructor.
     */
    public function __construct()
    {

    }

}