<?php
/**
 * Created by PhpStorm.
 * User: ernesto
 * Date: 28/7/2019
 * Time: 08:54
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Paquetes
 * @package App\Entity
 * @ORM\Entity
 * @ORM\Table(name="paquetes")
 *
 */
class Paquetes
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    protected $referidos=0;

    /**
     * @ORM\Column(type="float")
     */
    protected $saldo = 0;
    /**
     * @ORM\Column(type="float")
     */
    protected $ganancia = 0;
    /**
     * @ORM\Column(type="float")
     */
    protected $ganancia_mes = 0;
    /**
     * @ORM\OneToMany(targetEntity="MisPaquetes", mappedBy="user")
     */
    protected $mis_paquetes;
    /**
     * @ORM\Column(type="integer")
     */
    protected $hora=1;
    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    protected $dias_vida=0;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $oferta=0;

    /**
     * @return mixed
     */
    public function getOferta()
    {
        return $this->oferta;
    }

    /**
     * @param mixed $oferta
     * @return Paquetes
     */
    public function setOferta($oferta)
    {
        $this->oferta = $oferta;
        return $this;
    }
    /**
     * @return mixed
     */

    public function getGananciaMes()
    {
        return $this->ganancia_mes;
    }

    /**
     * @param mixed $ganancia_mes
     * @return Paquetes
     */
    public function setGananciaMes($ganancia_mes)
    {
        $this->ganancia_mes = $ganancia_mes;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiasVida()
    {
        return $this->dias_vida;
    }

    /**
     * @param mixed $dias_vida
     * @return Paquetes
     */
    public function setDiasVida($dias_vida)
    {
        $this->dias_vida = $dias_vida;
        return $this;
    }
    /**
     * Paquetes constructor.
     */
    public function __construct()
    {
        $this->mis_paquetes=new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getGanancia()
    {
        return $this->ganancia;
    }

    /**
     * @param mixed $ganancia
     * @return Paquetes
     */
    public function setGanancia($ganancia)
    {
        $this->ganancia = $ganancia;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMisPaquetes()
    {
        return $this->mis_paquetes;
    }

    /**
     * @param mixed $mis_paquetes
     * @return Paquetes
     */
    public function setMisPaquetes($mis_paquetes)
    {
        $this->mis_paquetes = $mis_paquetes;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getReferidos()
    {
        return $this->referidos;
    }

    /**
     * @param mixed $referidos
     */
    public function setReferidos($referidos): void
    {
        $this->referidos = $referidos;
    }

    /**
     * @return mixed
     */
    public function getSaldo()
    {
        return $this->saldo;
    }

    /**
     * @param mixed $saldo
     */
    public function setSaldo($saldo): void
    {
        $this->saldo = $saldo;
    }
     
	public function __toString(){
		return $this->id."";
	}

    /**
     * @return mixed
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * @param mixed $hora
     */
    public function setHora($hora): void
    {
        $this->hora = $hora;
    }

}