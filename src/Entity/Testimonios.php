<?php
/**
 * Created by PhpStorm.
 * User: ernesto
 * Date: 28/7/2019
 * Time: 09:38
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Testimonios
 * @package App\Entity
 * @ORM\Entity
 * @ORM\Table(name="testimonios")
 *
 */
class Testimonios
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="mistestimonios")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE"))
     */
    protected $user;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $fecha;
    /**
     * @ORM\Column(type="text")
     */
    protected $testimonio;
    /**
     * @ORM\Column(type="string")
     */
    protected $watsapp;
    /**
     * Testimonios constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getWatsapp()
    {
        return $this->watsapp;
    }

    /**
     * @param mixed $watsapp
     */
    public function setWatsapp($watsapp): void
    {
        $this->watsapp = $watsapp;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha): void
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getTestimonio()
    {
        return $this->testimonio;
    }

    /**
     * @param mixed $testimonio
     */
    public function setTestimonio($testimonio): void
    {
        $this->testimonio = $testimonio;
    }


}