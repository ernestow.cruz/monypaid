<?php

namespace App\Controller;

use App\Entity\MisPaquetes;
use App\Form\MisPaquetesType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/mis/paquetes")
 */
class MisPaquetesController extends AbstractController
{
    /**
     * @Route("/", name="mis_paquetes_index", methods={"GET"})
     */
    public function index(): Response
    {
        $misPaquetes = $this->getDoctrine()
            ->getRepository(MisPaquetes::class)
            ->findAll();

        return $this->render('mis_paquetes/index.html.twig', [
            'mis_paquetes' => $misPaquetes,
        ]);
    }

    /**
     * @Route("/new", name="mis_paquetes_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $misPaquete = new MisPaquetes();
        $form = $this->createForm(MisPaquetesType::class, $misPaquete);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($misPaquete);
            $entityManager->flush();

            return $this->redirectToRoute('mis_paquetes_index');
        }

        return $this->render('mis_paquetes/new.html.twig', [
            'mis_paquete' => $misPaquete,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="mis_paquetes_show", methods={"GET"})
     */
    public function show(MisPaquetes $misPaquete): Response
    {
        return $this->render('mis_paquetes/show.html.twig', [
            'mis_paquete' => $misPaquete,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="mis_paquetes_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, MisPaquetes $misPaquete): Response
    {
        $form = $this->createForm(MisPaquetesType::class, $misPaquete);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('mis_paquetes_index', [
                'id' => $misPaquete->getId(),
            ]);
        }

        return $this->render('mis_paquetes/edit.html.twig', [
            'mis_paquete' => $misPaquete,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="mis_paquetes_delete", methods={"DELETE"})
     */
    public function delete(Request $request, MisPaquetes $misPaquete): Response
    {
        if ($this->isCsrfTokenValid('delete'.$misPaquete->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($misPaquete);
            $entityManager->flush();
        }

        return $this->redirectToRoute('mis_paquetes_index');
    }
}
