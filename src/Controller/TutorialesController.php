<?php

namespace App\Controller;

use App\Entity\Tutoriales;
use App\Form\TutorialesType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tutoriales")
 */
class TutorialesController extends AbstractController
{
    /**
     * @Route("/", name="tutoriales_index", methods={"GET"})
     */
    public function index(): Response
    {
        $tutoriales = $this->getDoctrine()
            ->getRepository(Tutoriales::class)
            ->findAll();

        return $this->render('tutoriales/index.html.twig', [
            'tutoriales' => $tutoriales,
        ]);
    }

    /**
     * @Route("/new", name="tutoriales_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $tutoriale = new Tutoriales();
        $form = $this->createForm(TutorialesType::class, $tutoriale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tutoriale);
            $entityManager->flush();

            return $this->redirectToRoute('tutoriales_index');
        }

        return $this->render('tutoriales/new.html.twig', [
            'tutoriale' => $tutoriale,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tutoriales_show", methods={"GET"})
     */
    public function show(Tutoriales $tutoriale): Response
    {
        return $this->render('tutoriales/show.html.twig', [
            'tutoriale' => $tutoriale,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tutoriales_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Tutoriales $tutoriale): Response
    {
        $form = $this->createForm(TutorialesType::class, $tutoriale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tutoriales_index', [
                'id' => $tutoriale->getId(),
            ]);
        }

        return $this->render('tutoriales/edit.html.twig', [
            'tutoriale' => $tutoriale,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tutoriales_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Tutoriales $tutoriale): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tutoriale->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tutoriale);
            $entityManager->flush();
        }

        return $this->redirectToRoute('tutoriales_index');
    }
}
