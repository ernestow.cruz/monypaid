<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/usuarios")
 */
class UsuariosController extends AbstractController
{

    /**
     * @Route("/{id}", name="usuarios_list", methods={"GET"})
     */
    public function list_users():Response
    {
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();
        return $this->render('usuarios/list.html.twig', [
            'usuarios' => $user,
        ]);

    }


}
