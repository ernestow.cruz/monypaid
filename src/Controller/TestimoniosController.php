<?php

namespace App\Controller;

use App\Entity\Testimonios;
use App\Form\TestimoniosType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/testimonios")
 */
class TestimoniosController extends AbstractController
{
    /**
     * @Route("/", name="testimonios_index", methods={"GET"})
     */
    public function index(): Response
    {
        $testimonios = $this->getDoctrine()
            ->getRepository(Testimonios::class)
            ->findAll();

        return $this->render('testimonios/index.html.twig', [
            'testimonios' => $testimonios,
        ]);
    }

    /**
     * @Route("/new", name="testimonios_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $testimonio = new Testimonios();
        $form = $this->createForm(TestimoniosType::class, $testimonio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($testimonio);
            $entityManager->flush();

            return $this->redirectToRoute('testimonios_index');
        }

        return $this->render('testimonios/new.html.twig', [
            'testimonio' => $testimonio,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="testimonios_show", methods={"GET"})
     */
    public function show(Testimonios $testimonio): Response
    {
        return $this->render('testimonios/show.html.twig', [
            'testimonio' => $testimonio,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="testimonios_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Testimonios $testimonio): Response
    {
        $form = $this->createForm(TestimoniosType::class, $testimonio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('testimonios_index', [
                'id' => $testimonio->getId(),
            ]);
        }

        return $this->render('testimonios/edit.html.twig', [
            'testimonio' => $testimonio,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="testimonios_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Testimonios $testimonio): Response
    {
        if ($this->isCsrfTokenValid('delete'.$testimonio->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($testimonio);
            $entityManager->flush();
        }

        return $this->redirectToRoute('testimonios_index');
    }
}
