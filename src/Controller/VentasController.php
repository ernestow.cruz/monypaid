<?php

namespace App\Controller;

use App\Entity\Ventas;
use App\Form\VentasType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ventas")
 */
class VentasController extends AbstractController
{
    /**
     * @Route("/", name="ventas_index", methods={"GET"})
     */
    public function index(): Response
    {
        $ventas = $this->getDoctrine()
            ->getRepository(Ventas::class)
            ->findAll();

        return $this->render('ventas/index.html.twig', [
            'ventas' => $ventas,
        ]);
    }

    /**
     * @Route("/new", name="ventas_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $venta = new Ventas();
        $form = $this->createForm(VentasType::class, $venta);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($venta);
            $entityManager->flush();

            return $this->redirectToRoute('ventas_index');
        }

        return $this->render('ventas/new.html.twig', [
            'venta' => $venta,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ventas_show", methods={"GET"})
     */
    public function show(Ventas $venta): Response
    {
        return $this->render('ventas/show.html.twig', [
            'venta' => $venta,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="ventas_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Ventas $venta): Response
    {
        $form = $this->createForm(VentasType::class, $venta);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ventas_index', [
                'id' => $venta->getId(),
            ]);
        }

        return $this->render('ventas/edit.html.twig', [
            'venta' => $venta,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ventas_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Ventas $venta): Response
    {
        if ($this->isCsrfTokenValid('delete'.$venta->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($venta);
            $entityManager->flush();
        }

        return $this->redirectToRoute('ventas_index');
    }
}
