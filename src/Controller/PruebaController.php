<?php
/**
 * Created by PhpStorm.
 * User: ernesto
 * Date: 22/7/2019
 * Time: 09:02
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Entity\User;
use App\Form\UserType;

/**
     * @Route("/test")
     */
class PruebaController extends AbstractController
{
    /**
     * @Route("/register", name="prueba", methods={"POST"})
     */
    public function register(Request $request, SerializerInterface $serializer)
    {
       $user = new User();
        $form = $this->createForm(UserType::class,$user, array('csrf_protection'=>false));
	
        //$data =  $serializer->deserialize($request->getContent(), User::class,"json");
        $data = json_decode($request->getContent(),true);
	//return new Response($serializer->serialize($request->getContent(), 'json'));
	//return new Response(data);
        //var_dump($data);
        $form->submit($data);	
        if ($form->isSubmitted()&& $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
	    return new Response($serializer->serialize(array('message'=>'ok'), 'json'));
	    /*try {
		$em->flush();
		return new Response($serializer->serialize(array('message'=>'ok'), 'json'));
	    } catch (Exception $e){
		return new Response($serializer->serialize(array('message'=>'$e->getMessage()'), 'json'));
	    }*/
        }
        return new Response($serializer->serialize($form->getErrors(), 'json'));
	
    }
     /**
     * @Route("/registro_list", name="registro_list", methods={"GET"})
     */
    public function registro_list(Request $request, SerializerInterface $serializer)
    {
	$datos = array('mail'=>'ecruz@tm.cu','estado'=>'en espera','fecha'=>'10/10/2019','saldo'=>250);
	$datos1 = array('mail'=>'ecruz@tm.cu','estado'=>'en espera','fecha'=>'10/10/2019','saldo'=>250);
	$d = array($datos,$datos1);
	$response = new JsonResponse();
	$response->setData($d);
	//return $response;
	//return new Response(array('datos'=>json_encode($d)));
	return new Response($serializer->serialize(array('datos'=>$d), 'json'));
    }
}