<?php
/**
 * Created by PhpStorm.
 * User: ernesto
 * Date: 3/8/2019
 * Time: 04:42
 */

namespace App\Controller\Api;

use App\Controller\TutorialesController;
use App\Entity\Tutoriales;
use FOS\UserBundle\Model\UserManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * @Route("/api")
 */
class ApiTutorialesController extends AbstractController
{
    /** comprar un paquete
     * @Route("/tutoriales", name="tutoriales", methods={"GET"})
     */
    public function tutoriales(Request $request)
    {
        $datos = $this->getDoctrine()
            ->getRepository(Tutoriales::class)
            ->findAll();

        $aItems=array();
        foreach ($datos as $ref){
            $aItems[] = array('id'=>$ref->getId(),'tutorial'=>$ref->getTutorial(),'web'=>$ref->getWeb(),
                'fecha'=>$ref->getFecha());
        }
        return new JsonResponse(array('datos'=>$aItems));
    }

}