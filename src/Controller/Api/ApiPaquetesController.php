<?php
/**
 * Created by PhpStorm.
 * User: ernesto
 * Date: 24/7/2019
 * Time: 12:27
 */

namespace App\Controller\Api;


use App\Entity\MisPaquetes;
use App\Entity\Paquetes;
use App\Entity\User;
use App\Form\UserType;
use FOS\UserBundle\Model\UserManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/api")
 */
class ApiPaquetesController extends AbstractController
{
     /**
     * @Route("/paqueteslist", name="paquetes_list", methods={"GET"})
     */
    public function paquetes_list(Request $request)
    {
	$paquetes = $this->getDoctrine()
            ->getRepository(Paquetes::class)
            ->findAll();
	$aItems=array();
	foreach ($paquetes as $ref){
            $aItems[]=array('id'=>$ref->getId(),'referidos'=>$ref->getReferidos(),'saldo'=>$ref->getSaldo(),'oferta'=>$ref->getOferta(),
                'ganancia_dia'=>$ref->getGanancia(),'ganancia_mes'=>$ref->getGananciaMes(),'dias_vida'=>$ref->getDiasVida());
	}
	return new JsonResponse(array('datos'=>$aItems));
	//return new Response($serializer->serialize($paquetes, 'json'));
    }
    /** comprar un paquete
     * @Route("/{id}/comprar", name="comprar", methods={"POST"})
     */
    public function comprar(Request $request, Paquetes $paquete, JWTEncoderInterface $token,
                            UserManagerInterface $userManager): Response
    {
        try{
            $t=explode(' ',$request->headers->get('authorization'));
            $t=$token->decode($t[1]);//create($t[1]);
            $user = $userManager->findUserByUsername($t['username']);
        }catch (\Exception $exception) {
            return new JsonResponse(array('message'=>'La sesión se ha cerrado. Debe loguearse denuevo'));
        }
        $entityManager = $this->getDoctrine()->getManager();
		$sp = $user->getSaldocuc()- $paquete->getSaldo();
        if($sp >= 0) {
            $mi_paquete = new MisPaquetes();

            $mi_paquete->setPaquete($paquete);
            $f = new \DateTime();			
            $mi_paquete->setFecha($f);
            $mi_paquete->setFechaCal($f);
			$f1 = new \DateTime();
			$f1->add(new \DateInterval('P60D'));

            $mi_paquete->setVence($f1);
			$mi_paquete->setGanancia($paquete->getGanancia());
            $mi_paquete->setGananciaAcumulda($paquete->getGanancia());
			$mi_paquete->setUser($user);
            
            $user->setSaldocuc($sp);
            $user->setGanancia($user->getGanancia() + $paquete->getGanancia());
			$entityManager->persist($mi_paquete);
            $entityManager->flush();
            return new JsonResponse(array('message'=>'ok','saldo'=>$user->getSaldocuc()));
            //return new Response($serializer->serialize(array('message'=>'ok'), 'json'));
        }
        else{
			$sl = (string)$user->getSaldocuc();
            return new JsonResponse(array('message'=>'No tiene saldo para hacer esta operación('.$sl.')'));
            //return new Response($serializer->serialize(array('message'=>'No tiene saldo para hacer esta operación'), 'json'));
			}

    }
    /** list usuarios
     * @Route("/mis_paquetes", name="mis_paquetes", methods={"GET"})
     */
    public function mis_paquetes(Request $request,JWTEncoderInterface $token,
                                 UserManagerInterface $userManager): Response
    {
        try{
            $t=explode(' ',$request->headers->get('authorization'));
            $t=$token->decode($t[1]);//create($t[1]);
            $user = $userManager->findUserByUsername($t['username']);
        }catch (Exception $exception) {
            return new JsonResponse(array('message'=>'La sesión se ha cerrado. Debe loguearse denuevo'));
        }
        $aItems=array();
        foreach ($user->getMispaquetes() as $ref) {
            $aItems[] = array('id' => $ref->getId(), 'paquete' => $ref->getPaquete()->getId(),
                'ganancia' => $ref->getPaquete()->getGanancia(),'referidos' => $ref->getPaquete()->getReferidos(),
                'fecha' => $ref->getFecha(), 'vence' => $ref->getVence(),'ganancia_acumulada'=>$ref->getGananciaAcumulda());
        }
    return new JsonResponse(array('datos'=>$aItems));
	//return new Response($serializer->serialize(array('datos'=>$user->getMispaquetes()), 'json'));
    }
    
}