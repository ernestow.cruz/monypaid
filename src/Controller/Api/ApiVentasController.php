<?php
/**
 * Created by PhpStorm.
 * User: ernesto
 * Date: 3/8/2019
 * Time: 04:43
 */

namespace App\Controller\Api;

use App\Entity\Ventas;
use FOS\UserBundle\Model\UserManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * @Route("/api")
 */
class ApiVentasController extends AbstractController
{
    /** comprar un paquete
     * @Route("/ventas", name="ventas", methods={"GET"})
     */
    public function ventas(Request $request)
    {
        $informaciones = $this->getDoctrine()
            ->getRepository(Ventas::class)
            ->findAll();

        $aItems=array();
        foreach ($informaciones as $ref){
            $aItems[] = array('id'=>$ref->getId(),'nota'=>$ref->getNota(),'user'=>$ref->getUser()->getEmail(),
				'whatsapp'=>$ref->getUser()->getWhatsapp(),
                'fecha'=>$ref->getFecha());
        }
        return new JsonResponse(array('datos'=>$aItems));
    }
    /** comprar un paquete
     * @Route("/vender", name="vender", methods={"POST"})
     */
    public function vender(Request $request,  JWTEncoderInterface $token,
                               UserManagerInterface $userManager): Response
    {
        try{
            $t=explode(' ',$request->headers->get('authorization'));
            $t=$token->decode($t[1]);//create($t[1]);
            $user = $userManager->findUserByUsername($t['username']);
        }catch (\Exception $exception) {
            return new JsonResponse(array('message'=>'La sesión se ha cerrado. Debe loguearse denuevo'));
        }

        $data = json_decode($request->getContent(),true);
        if ($data['nota'] == '')
            return new JsonResponse(array('message'=>'Debe escribir una nota'));
        $venta = new Ventas();

        $entityManager = $this->getDoctrine()->getManager();
        $venta->setUser($user);
        $venta->setFecha( new \DateTime());
        $venta->setNota($data['nota']);
        $entityManager->persist($venta);
        $entityManager->flush();
        return new JsonResponse(array('message'=>'ok'));
    }

}