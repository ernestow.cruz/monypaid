<?php
/**
 * Created by PhpStorm.
 * User: ernesto
 * Date: 3/8/2019
 * Time: 04:42
 */

namespace App\Controller\Api;


use App\Entity\Informaciones;
use App\Entity\Testimonios;
use App\Entity\User;
use App\Form\TestimoniosType;
use App\Form\UserType;
use FOS\UserBundle\Model\UserManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * @Route("/api")
 */
class ApiInformacionesController extends AbstractController
{
    /** comprar un paquete
     * @Route("/informaciones", name="informaciones", methods={"GET"})
     */
    public function informaciones(Request $request)
    {
        $informaciones = $this->getDoctrine()
            ->getRepository(Informaciones::class)
            ->findAll();

        $aItems=array();
        foreach ($informaciones as $ref){
            $aItems[] = array('id'=>$ref->getId(),'nota'=>$ref->getNota(),
                'fecha'=>$ref->getFecha());
        }
        return new JsonResponse(array('datos'=>$aItems));
    }

}