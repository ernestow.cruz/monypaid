<?php
/**
 * Created by PhpStorm.
 * User: ernesto
 * Date: 24/7/2019
 * Time: 12:27
 */

namespace App\Controller\Api;


use App\Entity\Transferencias;
use App\Entity\User;
use App\Form\UserType;
use FOS\UserBundle\Model\UserManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/api")
 */
class ApiTransferenciasController extends AbstractController
{
    /**
     * @Route("/registros", name="registros_list", methods={"GET"})
     */
    public function registros(Request $request)
    {

        $transfer = $this->getDoctrine()
            ->getRepository(Transferencias::class)
            ->findAll();
        $aItems=array();
        foreach ($transfer as $ref){
            $aItems[]=array('id'=>$ref->getId(),'mail'=>$ref->getEmisor()->getEmail(),'estado'=>$ref->getEstado(),//
                'fecha'=>$ref->getFecha(),'saldo'=>$ref->getSaldo());
        }

     return new JsonResponse(array('datos'=>$aItems));

    }
    /** comprar un paquete
     * @Route("/transferir", name="transferir", methods={"POST"})
     */
    public function transferir(Request $request,  JWTEncoderInterface $token,
                               UserManagerInterface $userManager): Response
    {
        try{
            $t=explode(' ',$request->headers->get('authorization'));
            $t=$token->decode($t[1]);//create($t[1]);
            $emisor = $userManager->findUserByUsername($t['username']);
        }catch (\Exception $exception) {
            return new JsonResponse(array('message'=>'La sesión se ha cerrado. Debe loguearse denuevo'));
        }

        $data = json_decode($request->getContent(),true);
        if (!array_key_exists('version',$data))
            return new JsonResponse(array('message'=>'Debe actualizar su versión de la APK'));
        $saldo = $data['saldo'];
        if ($emisor->getSaldousd()<$saldo)
            return new JsonResponse(array('message'=>'No tiene saldo disponible'));

        $receptor=$userManager->findUserByEmail($data['email']) ;
        if(is_null($receptor))
            return new JsonResponse(array('message'=>'Correo no existe'));

        if ($saldo<10)
            return new JsonResponse(array('message'=>'La cantidad debe ser mayor que 10'));
        /*
        if(is_null($emisor->getFechatransferencia()))
            try {
                $emisor->setFechatransferencia(new \DateTime());

            } catch (\Exception $e) {
                return new JsonResponse(array('message'=>$e->getMessage()));
            }

        try {
            $ha = new \DateTime();
            $transfer_periodo = $this->getDoctrine()
            ->getRepository(Transferencias::class)
            ->getTransferenciasPeriodo($emisor,$emisor->getFechatransferencia(),$ha) ;
        } catch (\Exception $e) {
            return new JsonResponse(array('message'=>$e->getMessage()));
        }

        $interval = $ha->diff($emisor->getFechatransferencia());
        $hp = (Integer)$interval->format('%d');
        return new JsonResponse(array('message'=>$hp));

        $emisor->setTransferenciaPeriodo(getTransferenciaPeriodo()+ $saldo);*/
        $transfer = new Transferencias();
        $entityManager = $this->getDoctrine()->getManager();
        $transfer->setEmisor($emisor);
        $transfer->setReceptor($receptor);
        try {
            $transfer->setFecha(new \DateTime());
        } catch (\Exception $e) {
            return new JsonResponse(array('message'=>$e->getMessage()));
        }
        $transfer->setSaldo($saldo);
        $entityManager->persist($transfer);
        $entityManager->flush();
        return new JsonResponse(array('message'=>'ok'));
        //return new Response($serializer->serialize(array('message'=>'ok'), 'json'));
    }
    /** list usuarios
     * @Route("/mis_registros", name="mis_registros", methods={"GET"})
     */
    public function mis_registros(Request $request, JWTEncoderInterface $token,
                                  UserManagerInterface $userManager): Response
    {
        try{
            $t=explode(' ',$request->headers->get('authorization'));
            $t=$token->decode($t[1]);//create($t[1]);
            $emisor = $userManager->findUserByUsername($t['username']);
        }catch (\Exception $exception) {
            return new JsonResponse(array('message'=>'La sesión se ha cerrado. Debe loguearse denuevo'));
        }
        //$t=explode(' ',$request->headers->get('authorization'));
        //$t=$token->decode($t[1]);//create($t[1]);
        //$emisor=$userManager->findUserByUsername($t['username']);

        $paquetes = $this->getDoctrine()
            ->getRepository(Transferencias::class)
            ->getAllTransfers($emisor,$emisor) ;
		$aItems=array();
        foreach ($paquetes as $ref){
            if ($ref->getEmisor())$e_emisor = $ref->getEmisor()->getEmail();
            else
                $e_emisor = '';
            if ($ref->getReceptor())$e_receptor = $ref->getReceptor()->getEmail();
            else
                $e_receptor = '';
            $aItems[]=array('id'=>$ref->getId(),'emisor'=>$e_emisor,'estado'=>$ref->getEstado(),
                'receptor'=>$e_receptor,
                'fecha'=>$ref->getFecha(),'saldo'=>$ref->getSaldo());
        }
    return new JsonResponse(array('datos'=>$aItems));
	//return new Response($serializer->serialize(array('datos'=>$aItems), 'json'));
    }
    /** comprar un paquete
     * @Route("/confirmar/{id}", name="confirmar", methods={"POST"})
     */
    public function confirmar(Request $request,Transferencias $transferencia,
                              JWTEncoderInterface $token,
                              UserManagerInterface $userManager): Response
    {
        try{
            $t=explode(' ',$request->headers->get('authorization'));
            $t=$token->decode($t[1]);//create($t[1]);
            $user = $userManager->findUserByUsername($t['username']);
        }catch (\Exception $exception) {
            return new JsonResponse(array('message'=>'La sesión se ha cerrado. Debe loguearse denuevo'));
        }
        //try{
        $data = json_decode($request->getContent(),true);
        if (is_null($data))
            return new JsonResponse(array('message'=>'Debe actualizar su versión de la APK'));
        //if (!array_key_exists('version',$data))
        //    return new JsonResponse(array('message'=>'Debe actualizar su versión de la APK'));
        /*}catch (\Exception $exception) {
            return new JsonResponse(array('message'=>$exception->getMessage()));
        }*/
        if ($transferencia->getEstado() === 'confirmado')
            return new JsonResponse(array('message'=>'La transferencia ya está confirmada'));


        $emisor = $transferencia->getEmisor();
        if($user->getId() == $emisor->getId())
            return new JsonResponse(array('message'=>'Usted no puede confirmar su transferencia'));

        $sa = $emisor->getSaldousd()- $transferencia->getSaldo();
        if ($sa<0)//si no tiene saldo
            return new JsonResponse(array('message'=>'No tiene saldo disponible'));
            //return new Response($serializer->serialize(array('message'=>'No tiene saldo disponible'), 'json'));
        try{
            //$emisor->setSaldousd($sa);
            //$user->setSaldocuc($user->getSaldocuc() + $transferencia->getSaldo());
            $transferencia->setEstado('confirmado');
            $transferencia->setFecha(new \DateTime());
            $entityManager = $this->getDoctrine()->getManager();
            //$entityManager->persist($user);
            $entityManager->flush();
            return new JsonResponse(array('message'=>'ok'));
        }
        catch (\Exception $exception) {
            return new JsonResponse(array('message'=>$exception->getMessage()));
        }
        //return new Response($serializer->serialize(array('message'=>'ok'), 'json'));
    }

    /** comprar un paquete
     * @Route("/cerrar/{id}", name="cerrar", methods={"POST"})
     */
    public function cerrar(Request $request,Transferencias $transferencia,
                              JWTEncoderInterface $token,
                              UserManagerInterface $userManager): Response
    {
        try{
            $t=explode(' ',$request->headers->get('authorization'));
            $t=$token->decode($t[1]);//create($t[1]);
            $user = $userManager->findUserByUsername($t['username']);
        }catch (\Exception $exception) {
            return new JsonResponse(array('message'=>'La sesión se ha cerrado. Debe loguearse denuevo'));
        }
        $data = json_decode($request->getContent(),true);
        if (is_null($data))
            return new JsonResponse(array('message'=>'Debe actualizar su versión de la APK'));

        if ($transferencia->getEstado() === 'cerrado')
            return new JsonResponse(array('message'=>'La transferencia ya está cerrada'));

        if ($transferencia->getEstado() === 'confirmado'){
            $emisor = $transferencia->getEmisor();
            if($user->getId() != $emisor->getId())
                return new JsonResponse(array('message'=>'Usted no puede cerrar esta transferencia solo '.$emisor->getEmail().' puede hacerlo'));

            $sa = $emisor->getSaldousd()- $transferencia->getSaldo();
            if ($sa<0)//si no tiene saldo
                return new JsonResponse(array('message'=>'No tiene saldo disponible'));
            //return new Response($serializer->serialize(array('message'=>'No tiene saldo disponible'), 'json'));
            try{
                $emisor->setSaldousd($sa);

                $receptor = $transferencia->getReceptor();
                $receptor->setSaldocuc($receptor->getSaldocuc() + $transferencia->getSaldo());
                $transferencia->setEstado('cerrado');
                $transferencia->setFecha(new \DateTime());
                $entityManager = $this->getDoctrine()->getManager();
                //$entityManager->persist($user);
                $entityManager->flush();
                return new JsonResponse(array('message'=>'ok'));
            }
            catch (\Exception $exception) {
                return new JsonResponse(array('message'=>$exception->getMessage()));
            }
        }
        else
            return new JsonResponse(array('message'=>'La transferencia debe estár confirmada'));
    }

    /** comprar un paquete
     * @Route("/noconfirmar/{id}", name="noconfirmar", methods={"POST"})
     */
    public function noconfirmar(Request $request,Transferencias $transferencia,
                              JWTEncoderInterface $token,
                              UserManagerInterface $userManager): Response
    {
        try{
            $t=explode(' ',$request->headers->get('authorization'));
            $t=$token->decode($t[1]);//create($t[1]);
            $user = $userManager->findUserByUsername($t['username']);
        }catch (Exception $exception) {
            return new JsonResponse(array('message'=>'La sesión se ha cerrado. Debe loguearse denuevo'));
        }
        if ($transferencia->getEstado() === 'no confirmado')
            return new JsonResponse(array('message'=>'La transferencia ya está cancelda'));

        $emisor = $transferencia->getEmisor();
        $receptor = $transferencia->getReceptor();
        if ($transferencia->getEstado() === 'en espera'){
            if($user->getId() == $emisor->getId())
                return new JsonResponse(array('message'=>'Usted no puede cancelar la transferencia solo '.$receptor->getEmail()));
        }
        else
            if ($transferencia->getEstado() === 'confirmado'){
                if($user->getId() == $receptor->getId())
                    return new JsonResponse(array('message'=>'Usted no puede cancelar la transferencia solo '.$emisor->getEmail()));
            }
        $transferencia->setEstado('no confirmado');
        $transferencia->setFecha(new \DateTime());
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();
        return new JsonResponse(array('message'=>'ok'));
    }


}