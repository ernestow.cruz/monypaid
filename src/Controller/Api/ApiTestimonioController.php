<?php
/**
 * Created by PhpStorm.
 * User: ernesto
 * Date: 24/7/2019
 * Time: 12:27
 */

namespace App\Controller\Api;


use App\Entity\Testimonios;
use FOS\UserBundle\Model\UserManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * @Route("/api")
 */
class ApiTestimonioController extends AbstractController
{
    /** comprar un paquete
     * @Route("/testimonios", name="testimonios", methods={"GET"})
     */
    public function testimonios(Request $request)
    {
        $testimonios = $this->getDoctrine()
            ->getRepository(Testimonios::class)
            ->findAll();
		
		$aItems=array();
        foreach ($testimonios as $ref){
            $aItems[] = array('id'=>$ref->getId(),'username'=>$ref->getUser()->getEmail(),
                'testimonio'=>$ref->getTestimonio(), 'fecha'=>$ref->getFecha());
		}
	//return new Response($serializer->serialize(array('datos'=>$aItems), 'json'));
    return new JsonResponse(array('datos'=>$aItems));
    }
    /** comprar un paquete
     * @Route("/testificar", name="testificar", methods={"POST"})
     */
    public function testificar(Request $request,  JWTEncoderInterface $token,
                               UserManagerInterface $userManager): Response
    {
        try{
            $t=explode(' ',$request->headers->get('authorization'));
            $t=$token->decode($t[1]);//create($t[1]);
            $user = $userManager->findUserByUsername($t['username']);
        }catch (Exception $exception) {
            return new JsonResponse(array('message'=>'La sesión se ha cerrado. Debe loguearse denuevo'));
        }

        $testim = new Testimonios();
        $data = json_decode($request->getContent(),true);
        $entityManager = $this->getDoctrine()->getManager();
        $testim->setUser($user);
        $testim->setFecha( new \DateTime());
        $testim->setWatsapp($data['watsapp']);
        $testim->setTestimonio($data['testimonio']);
        $entityManager->persist($testim);
        $entityManager->flush();
        return new JsonResponse(array('message'=>'ok'));
    }
    
}