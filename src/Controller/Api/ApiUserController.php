<?php
/**
 * Created by PhpStorm.
 * User: ernesto
 * Date: 24/7/2019
 * Time: 12:27
 */

namespace App\Controller\Api;


use App\Entity\MisPaquetes;
use App\Entity\User;
use App\Form\UserType;
use FOS\UserBundle\Model\UserManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/api")
 */
class ApiUserController extends AbstractController
{
    /** crear un usuario
     * @Route("/register", name="register", methods={"POST"})
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder): Response //,
    {
        try {

            $data = json_decode($request->getContent(), true);
            //return new JsonResponse(array('message' => $data['patrocinador']));
            if($data['patrocinador'] != -1){
                $patron = $this->getDoctrine()
                    ->getRepository(User::class)
                    ->find($data['patrocinador']);

                if (is_null($patron))
                    return new JsonResponse(array('message' => 'El patrocindor no existe'));
                if (count($patron->getMispaquetes())>0)
                    $patron->setGanancia($patron->getGanancia()+1);
            }
            $user = new User();
            $entityManager = $this->getDoctrine()->getManager();
            $user->setUsername($data['username']);
            $user->setPassword($encoder->encodePassword($user, $data['password']));
            $user->setEmail($data['email']);
            $user->setPatrocinador($data['patrocinador']);
            $user->setEnabled(true);
            $entityManager->persist($user);
            $entityManager->flush();
            return new JsonResponse(array('message' => 'ok'));
        }catch (\Exception $e){
            return new JsonResponse(array('message' => $e->getMessage()));
        }
        //
    }
    /** list usuarios
     * @Route("/userlist", name="user_list", methods={"GET"})
     */
    public function userlist(Request $request): Response
    {    
        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();
        $aItems=array();
        foreach ($users as $ref) {
            $aItems[] = array('id' => $ref->getId(), 'username' => $ref->getUsername(),'email'=>$ref->getEmail(),
                'usd' => $ref->getSaldousd(),'cuc'=>$ref->getSaldocuc(),'pais'=>$ref->getPais()
                );
        }
    return new JsonResponse(array('datos'=>$aItems));
	//return new Response($serializer->serialize(array('datos'=>$users), 'json'));
    }
    /**
     * @Route("/{id}/referidos", name="user_referidos", methods={"GET"})
     */
    public function referidos(Request $request,  User $user): Response
    {
        $referidos = $this->getDoctrine()
            ->getRepository(User::class)
            ->findBy(['patrocinador'=>$user->getId()]);
        $aItems=array();
        foreach ($referidos as $ref) {
            $aItems[] = array('id' => $ref->getId(), 'username' => $ref->getUsername(),'email'=>$ref->getEmail(),
                'usd' => $ref->getSaldousd(),'cuc'=>$ref->getSaldocuc(),'pais'=>$ref->getPais()
            );
        }
     return new JsonResponse(array('datos'=>$aItems));
	//return new Response($serializer->serialize(array('datos'=>$referidos), 'json'));
    }
    /**
     * @Route("/mis_referidos", name="mis_referidos", methods={"GET"})
     */
    public function mis_referidos(Request $request, JWTEncoderInterface $token,
                                  UserManagerInterface $userManager): Response
    {
        try{
            $t=explode(' ',$request->headers->get('authorization'));
            $t=$token->decode($t[1]);//create($t[1]);
            $user = $userManager->findUserByUsername($t['username']);
        }catch (Exception $exception) {
            return new JsonResponse(array('message'=>'La sesión se ha cerrado. Debe loguearse denuevo'));
        }

        $referidos = $this->getDoctrine()
            ->getRepository(User::class)
            ->findBy(['patrocinador'=>$user->getId()]);

        $aItems=array();
        foreach ($referidos as $ref){
            $aItems[]=array('id'=>$ref->getId(),'username' => $ref->getUsername(),'email'=>$ref->getEmail(),
                'usd'=>$ref->getSaldousd(),'cuc'=>$ref->getSaldocuc(),'pais'=>$ref->getPais(),
                'activo'=>(count($ref->getMispaquetes())>0));
        }
    return new JsonResponse(array('datos'=>$aItems));
	//return new Response($serializer->serialize(array('datos'=>$aItems), 'json'));
    }
    /** comprar un paquete
     * @Route("/dashboard", name="dashboard", methods={"GET"})
     */
    public function dashboard(Request $request,
                             JWTEncoderInterface $token,
                             UserManagerInterface $userManager
    ): Response
    {
        try{
            $t=explode(' ',$request->headers->get('authorization'));
            $t=$token->decode($t[1]);//create($t[1]);
            $user = $userManager->findUserByUsername($t['username']);
        }catch (Exception $exception) {
            return new JsonResponse(array('message'=>'La sesión se ha cerrado. Debe loguearse denuevo'));
        }
        $referidos = $this->getDoctrine()
            ->getRepository(User::class)
            ->findBy(['patrocinador'=>$user->getId()]);
        //revisar para mejorar
        $gpd = 0;
        foreach ($user->getMispaquetes() as $pack) {
            $gpd = $gpd + $pack->getPaquete()->getGanancia();
        }
        $aItems=array();
        $aItems[] = array('id'=>$user->getId(),'usd'=>$user->getSaldousd(),'cuc'=>$user->getSaldocuc(),
        'ganancia'=>$gpd,'referidos'=>count($referidos));
        return new JsonResponse(array('datos'=>$aItems));
        //return new Response($serializer->serialize(array('datos'=>$aItems), 'json'));
    }
    /** comprar un paquete
     * @Route("/perfil", name="dasboard", methods={"GET"})
     */
    public function perfil(Request $request,
                              JWTEncoderInterface $token,
                              UserManagerInterface $userManager
    ): Response
    {
        try{
            $t=explode(' ',$request->headers->get('authorization'));
            $t=$token->decode($t[1]);//create($t[1]);
            $user = $userManager->findUserByUsername($t['username']);
        }catch (\Exception $exception) {
            return new JsonResponse(array('message'=>'La sesión se ha cerrado. Debe loguearse denuevo'));
        }
        $aItems=array();
        $aItems[] = array('id'=>$user->getId(),'username'=>$user->getUsername(),'email'=>$user->getEmail(),
            'pais'=>$user->getPais(),'whatsapp'=>$user->getWhatsapp());
        return new JsonResponse(array('datos'=>$aItems));
        //return new Response($serializer->serialize(array('datos'=>$aItems), 'json'));
    }
    /** modifica el perfil
     * @Route("/perfilset", name="user_register", methods={"POST"})
     */
    public function perfilset(Request $request, UserPasswordEncoderInterface $encoder,
                              JWTEncoderInterface $token, UserManagerInterface $userManager): Response
    {
        try{
            $t=explode(' ',$request->headers->get('authorization'));
            $t=$token->decode($t[1]);//create($t[1]);
            $user = $userManager->findUserByUsername($t['username']);
        }catch (\Exception $exception) {
            return new JsonResponse(array('message'=>'La sesión se ha cerrado. Debe loguearse denuevo'));
        }

        $data = json_decode($request->getContent(), true);
        $entityManager = $this->getDoctrine()->getManager();
        $user->setUsername($data['username']);
        if (!$data['password']==='')
            $user->setPassword($encoder->encodePassword($user, $data['password']));
        $user->setEmail($data['email']);

        $user->setPais($data['pais']);
        $user->setWhatsapp($data['whatsapp']);
        $user->setEnabled(true);
        $entityManager->persist($user);
        $entityManager->flush();
        return new JsonResponse(array('message' => 'ok'));

    }

}