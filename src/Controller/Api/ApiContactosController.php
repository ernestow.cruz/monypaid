<?php
/**
 * Created by PhpStorm.
 * User: ernesto
 * Date: 3/8/2019
 * Time: 04:43
 */

namespace App\Controller\Api;

use App\Entity\Contactos;
use App\Entity\MisPaquetes;
use FOS\UserBundle\Model\UserManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Console\Output\BufferedOutput;
/**
 * @Route("/api")
 */
class ApiContactosController extends AbstractController
{
    /** comprar un paquete
     * @Route("/contactos", name="contactos", methods={"GET"})
     */
    public function contactos(Request $request)
    {
        $testimonios = $this->getDoctrine()
            ->getRepository(Contactos::class)
            ->findAll();

        $aItems=array();
        foreach ($testimonios as $ref){
            $aItems[] = array('id'=>$ref->getId(),'nombre'=>$ref->getNombre(),'email'=>$ref->getEmail());
        }
        //return new Response($serializer->serialize(array('datos'=>$aItems), 'json'));
        return new JsonResponse(array('datos'=>$aItems));
    }

}