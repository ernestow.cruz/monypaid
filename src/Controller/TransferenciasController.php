<?php

namespace App\Controller;

use App\Entity\Transferencias;
use App\Form\TransferenciasType;
use App\Repository\TransferenciasRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/transferencias")
 */
class TransferenciasController extends AbstractController
{
    /**
     * @Route("/", name="transferencias_index", methods={"GET"})
     */
    public function index(): Response
    {
        $transferencias = $this->getDoctrine()
            ->getRepository(Transferencias::class)
            ->findAll();
        return $this->render('transferencias/index.html.twig', [
            'transferencias' => $transferencias,
        ]);
    }

    /**
     * @Route("/new", name="transferencias_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $transferencia = new Transferencias();
        $form = $this->createForm(TransferenciasType::class, $transferencia);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($transferencia);
            $entityManager->flush();

            return $this->redirectToRoute('transferencias_index');
        }

        return $this->render('transferencias/new.html.twig', [
            'transferencia' => $transferencia,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="transferencias_show", methods={"GET"})
     */
    public function show(Transferencias $transferencia): Response
    {
        return $this->render('transferencias/show.html.twig', [
            'transferencia' => $transferencia,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="transferencias_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Transferencias $transferencia): Response
    {
        $form = $this->createForm(TransferenciasType::class, $transferencia);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('transferencias_index', [
                'id' => $transferencia->getId(),
            ]);
        }

        return $this->render('transferencias/edit.html.twig', [
            'transferencia' => $transferencia,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="transferencias_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Transferencias $transferencia): Response
    {
        if ($this->isCsrfTokenValid('delete'.$transferencia->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($transferencia);
            $entityManager->flush();
        }

        return $this->redirectToRoute('transferencias_index');
    }
}
