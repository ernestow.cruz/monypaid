<?php

namespace App\Controller;

use App\Entity\Informaciones;
use App\Form\InformacionesType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/informaciones")
 */
class InformacionesController extends AbstractController
{
    /**
     * @Route("/", name="informaciones_index", methods={"GET"})
     */
    public function index(): Response
    {
        $informaciones = $this->getDoctrine()
            ->getRepository(Informaciones::class)
            ->findAll();

        return $this->render('informaciones/index.html.twig', [
            'informaciones' => $informaciones,
        ]);
    }

    /**
     * @Route("/new", name="informaciones_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $informacione = new Informaciones();
        $form = $this->createForm(InformacionesType::class, $informacione);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($informacione);
            $entityManager->flush();

            return $this->redirectToRoute('informaciones_index');
        }

        return $this->render('informaciones/new.html.twig', [
            'informacione' => $informacione,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="informaciones_show", methods={"GET"})
     */
    public function show(Informaciones $informacione): Response
    {
        return $this->render('informaciones/show.html.twig', [
            'informacione' => $informacione,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="informaciones_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Informaciones $informacione): Response
    {
        $form = $this->createForm(InformacionesType::class, $informacione);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('informaciones_index', [
                'id' => $informacione->getId(),
            ]);
        }

        return $this->render('informaciones/edit.html.twig', [
            'informacione' => $informacione,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="informaciones_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Informaciones $informacione): Response
    {
        if ($this->isCsrfTokenValid('delete'.$informacione->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($informacione);
            $entityManager->flush();
        }

        return $this->redirectToRoute('informaciones_index');
    }
}
