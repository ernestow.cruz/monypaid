<?php
/**
 * Created by PhpStorm.
 * User: ernesto
 * Date: 5/8/2019
 * Time: 12:45
 */

namespace App\Command;


use App\Entity\MisPaquetes;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ChequearPaquetesVencidos extends Command
{
    protected static $defaultName="money:check_pack_vencido";
    private $em;

    public function __construct(ObjectManager $em)
    {
        parent::__construct();
        $this->em=$em;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em->getRepository(MisPaquetes::class)->deleteAllVencidos(new \DateTime());
    }

}