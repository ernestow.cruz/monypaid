<?php
/**
 * Created by PhpStorm.
 * User: ernesto
 * Date: 9/8/2019
 * Time: 07:32
 */

namespace App\Command;

use App\Entity\MisPaquetes;
use Doctrine\Common\Persistence\ObjectManager;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ActualizarGananciasPaquetes extends Command
{
    protected static $defaultName="money:check_pack_ganancia";
    private $em;

    public function __construct(ObjectManager $em)
    {
        parent::__construct();
        $this->em=$em;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $ha = new \DateTime();
        $paquetes = $this->em->getRepository(MisPaquetes::class)->findAll();
        foreach ($paquetes as $pack){
            $user = $pack->getUser();
            /*echo 'hora del paquete:(',$pack->getFechaCal()->format("H:i,s").')';
            $interval = $ha->diff($pack->getFechaCal());
            $hp = (Integer)$interval->format('%H');
            //echo '*****['.$hp.'](',$interval->format('%H:%i,%s').')';
            //echo 'tiempo de calculo',$pack->getPaquete()->getHora();
            if ($hp >= $pack->getPaquete()->getHora()){
                $ganancia = intdiv($hp,$pack->getPaquete()->getHora())* $pack->getPaquete()->getGanancia();
                //echo 'ganancia<'.$ganancia.'>';*/
            $pack->setGananciaAcumulda($pack->getGananciaAcumulda() + $pack->getPaquete()->getGanancia());
            $user->setGanancia($user->getGanancia() + $pack->getPaquete()->getGanancia());
            $user->setSaldousd($user->getSaldousd() + $pack->getPaquete()->getGanancia());
            $pack->setFechaCal(new \DateTime());
            //$this->em->persist($pack);
            //$this->em->persist($user);
            //echo 'ganancia',$user->getGanancia() + $pack->getPaquete()->getGanancia();
            //}
        }

        $this->em->flush();
    }
}