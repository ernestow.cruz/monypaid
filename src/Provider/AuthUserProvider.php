<?php
/**
 * Created by PhpStorm.
 * User: ernesto
 * Date: 28/7/2019
 * Time: 09:19
 */
 
namespace App\Provider;
use FOS\UserBundle\Doctrine\UserManager;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class AuthUserProvider implements UserProviderInterface
{

    /**
     * @var UserRepository
     */
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    public function loadUserByUsername($username)
    {
        $foundedUser = $this->userManager->findUserByUsernameOrEmail($username);

        if ($foundedUser === null) {
            throw new UsernameNotFoundException();
        }

        return $foundedUser;
    }

    public function refreshUser(UserInterface $user)
    {
        // TODO: Implement refreshUser() method.
    }

    public function supportsClass($class)
    {
        // TODO: Implement supportsClass() method.
    }
}